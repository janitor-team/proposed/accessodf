UserGuide   

This user guide was taken from
  http://sourceforge.net/p/accessodf/wiki/UserGuide/
please have a look there for an up-to-date version.

User Guide

1. Introduction

AccessODF helps authors evaluate and improve the accessibility of their documents in
OpenOffice.org or LibreOffice Writer. A document may contain many problems that the author
is unaware of but that can keep someone with a disability from accessing the content. This
is true whether documents are accessed in Writer itself, or exported to a more common format
like HTML or PDF. Thanks to the extensions odt2daisy and odt2braille, Writer documents can
be a suitable source for formats specifically aimed at persons with disabilities. But in
order to create valid and usable Braille or DAISY books, the source document needs to be
accessible.

You can do several things to increase the accessibility of ODT documents. The following
documents give you a number of guidelines:

  • ODF Accessibility Guidelines by the Accessibility subcommittee of the OASIS Open
    Document Format for Office Applications (OpenDocument) TC
  • Authoring Techniques For Accessible Office Documents by the Accessible Digital Office
    Document (ADOD) Project
  • OpenOffice.org and Accessibility by Web Accessibility in Mind (WebAIM)

AccessODF helps you to keep these rules in mind and makes it very easy to write accessible
documents. Checking the accessibility is now as straightforward as checking the spelling:
similar to the spelling checker alerting you to potential spelling errors, the accessibility
checker helps you identify and resolve potential accessibility issues.

2. Installation

AccessODF is available as an extension. The installation requires OpenOffice.org/LibreOffice
3.3. AccessODF runs on all platforms (Windows, Mac OS, Linux).

 1. Download the installation file (OXT)
 2. In Writer, go to Tools > Extension Manager...
 3. Click Add...
 4. Select the OXT file and click Open
 5. Wait until the installation has finished, then click Close
 6. Finally, restart Writer

Warning: If you get an error dialog with the words
com.sun.star.registry.CannotRegisterImplementationException in it when installing the
extension on Ubuntu, open a command line and enter the command sudo apt-get install
libreoffice-java-common. This solution may also work for other Linux distributions.

3. Evaluate accessibility

The accessibility checker is shown in the Accessibility section of the Task Pane in Writer.
The Accessibility section of the Task Pane
You might have seen this Task Pane before in Impress, where it is used for Master Pages,
Layouts, Custom Animations and Transitions. It is usually located at the right side of the
workspace, but it can also be placed at the left or even undocked. To check the document for
possible issues, do the following:

 1. Open the document in Writer
 2. Go to Tools > Accessibility Evaluation... The Accessibility Evaluation item in the Tools
    menu
 3. Click Check The check button
 4. Wait for the tool to scan the whole document

The task pane displays all issues in a 'tree' view. While navigating the issues, focus will
move to the relevant location in the document. Each issue is classified as an error or a
warning. Selecting an issue shows information about why something might be inaccessible, and
instructions on how to fix it.
The accessibility checker gives an overview of all issues in a tree view
You can include or exclude certain checks specific to the export to DAISY and Braille. To
show these options, go to Tools > Options... > OpenOffice.org/LibreOffice Writer >
Accessibility Checker. In order to do Braille-specific checks, odt2braille version 1.1.0 or
later must be installed.
The Options page

4. Resolve accessibility issues

Each error or warning includes instructions on how to fix it manually. For some issues, you
can also use the handy Repair button. This button either fixes the issue automatically, or
it sends you in the right direction so that you can skip a few steps. It is also possible
that the checker reports an issue where no problem exists. In this case, just ignore the
issue with the Ignore button.

 1. Select an issue
 2. If this is not a real issue, click Ignore
 3. Else, if the Repair button is enabled, use it to fix the issue (semi-)automatically
 4. Else, fix the issue manually
 5. Repeat this until no issues are left

